const express = require("express")
const fs = require("fs")
const multer = require("multer");
const path = require("path")

//Set Storage Engine
const storage = multer.diskStorage({
    destination: './public/uploads',
    filename: function(req, file, callback){
        callback(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
});


//Check File Type

function checkFileType(file, callback){
    //Allowed extensions
    const fileTypes = /jpeg|jpg|png|gif/;
    //Check Ext
    const extName = fileTypes.test(path.extname(file.originalname).toLowerCase());
    //Check mime
    const mimeType = fileTypes.test(file.mimetype)
    if(mimeType && extName){
        return callback(null, true);
    } else {
        callback("Error: Images Only")
    }
}


const upload = multer({
    storage: storage, 
    fileFilter: function(req, file, callback){
        checkFileType(file, callback);
    }
}).single('myFile');



//Public Folder
function displayImages(imgNames){
    let output = "";
    for(let nameIndex=0; nameIndex<imgNames.length; nameIndex++){
        let names = imgNames[nameIndex];
        output += `<img src='../uploads/${names}'>`
        console.log(output)
        
    }
    return output
    
}


//Init app

const app = express();

app.use(express.static('./public'));

const port = 3000;


app.get("/", function(req, res){
    const filePath = './public/uploads';
    fs.readdir(filePath, function(err, items) {
        let catalogue = items.reverse();
        
        if (err){
            
            console.log(err.message)
        }
        else{
        console.log(items);
        res.send(`<h1>Welcome to Kenziegram!</h1>
        <form action="/upload" method="POST" enctype="multipart/form-data">
        <label name=file>Upload Your File Here:</label><br>
        <input type="file" name="myFile"/>
        <input type="submit" value="Submit"/>
        </form> 
        ${displayImages(catalogue)}`
        );
        }
        
    })
});

app.listen(port, () => console.log(`Server started on port ${port}`))

//Post



app.post('/upload', upload, function (request, response, next) {  //end points aren't file paths
    // request.file is the \`myFile\` file
    // request.body will hold the text fields, if there were any 
    let uploaded_files = []; 
    uploaded_files.push(request.file.filename);
    console.log("Uploaded: " + request.file.filename);
    console.log(uploaded_files)  
    response.end(`<h1>Uploaded file!</h1>
                    <br><br>
                    <a href="http://localhost:3000">BACK</a>
                    <br><br>
                    ${displayImages(uploaded_files)}`);
  });